const gulp = require('gulp')
const browserSync = require('browser-sync').create()
const reload = browserSync.reload

gulp.task('browser-sync', function () {
  browserSync.init({
    notify: false,
    server: {
      baseDir: './'
    }
  })
  gulp.watch('./*.html', reload);
  gulp.watch('./css/*.css', reload);
  gulp.watch('./js/*.js', reload);
})


gulp.task('default', ['browser-sync'])