class Food {
  constructor() {
    this.FoodList = {};
    this.searchActive = false;
    this.selectList = [];
    this.totalPrice = 0;
  }

  async getFoods() {
    const file = './data/menuData.json';
    const res = await fetch(file).then((res) => { return res.json() });
    this.FoodList = res.d.ResultSet;

    const boughtProductsList = document.getElementById('ListTotal');
    const empytBag = document.getElementById('emptyBag');

    if (this.selectList.length == 0) {
      boughtProductsList.style.display = 'none';
      empytBag.style.display = 'block';
    } else {
      boughtProductsList.style.display = 'block';
      empytBag.style.display = 'none';
    }
  }

  itemControl(item, searchKey) {
    let count = 0;
    item.forEach(element => {
      element.DisplayName.search(searchKey) > -1 ? count++ : count;
    });
    return count === 0 ? false : true;
  };

  dataFilter(searchKey) {
    return this.FoodList.filter
      (item => item.CategoryDisplayName.search(searchKey) > -1 || this.itemControl(item.Products, searchKey));
  }

  onSearch() {
    const searchValue = document.getElementById('searchInYemekSepeti').value;
    this.searchActive = true;
    const resultList = document.getElementById('searchResult');
    resultList.innerHTML = '';
    if (searchValue.length > 0) {
      const key = new RegExp(searchValue, 'gi');
      const filterList = this.dataFilter(key);
      this.showResult(filterList);
    }
  }
  getSelectList() {
    const productCheckList = document.querySelectorAll('.category .list-item input[name=productList]');
    const ProductCount = document.querySelectorAll('input[name=productCount]');
    for (let i = 0; i < productCheckList.length; i++) {
      productCheckList[i].addEventListener('change', () => {
        let pushData = {
          elId: productCheckList[i].getAttribute("id"),
          id: productCheckList[i].getAttribute("dataid"),
          name: productCheckList[i].getAttribute("dataname"),
          value: parseFloat(productCheckList[i].value.replace(/,/, '.')),
          count: parseInt(document.getElementById(productCheckList[i].getAttribute("datacount")).value)
        };
        if (productCheckList[i].checked) {
          this.selectList.push(pushData);
          pushData.count = 1;
          pushData.value *= pushData.count;
          document.getElementById(productCheckList[i].getAttribute("datacount")).value = 1;
          this.addBoughtList(pushData);
        }
        else {
          let sliceData = this.selectList.find(item => item.id == pushData.id);
          if (sliceData != null) {
            this.selectList.splice(this.selectList.indexOf(sliceData), 1);
          }
          document.getElementById(productCheckList[i].getAttribute("datacount")).value = 0;
          this.removeBoughtItem(pushData.id);
        }
        this.showListCount();
      });

    }

    for (let i = 0; i < ProductCount.length; i++) {
      ProductCount[i].addEventListener('change', () => {
        const checkDoc = document.querySelector(`input[datacount=${ProductCount[i].getAttribute('id')}]`);
        if (ProductCount[i].value && ProductCount[i].value != 0 && checkDoc.checked) {
          let hasData = {
            elId: checkDoc.getAttribute("id"),
            id: checkDoc.getAttribute("dataid"),
            name: checkDoc.getAttribute("dataname"),
            value: parseInt(ProductCount[i].value) * parseFloat(checkDoc.value.replace(/,/, '.')),
            count: parseInt(ProductCount[i].value)
          };
          this.selectList.splice(this.selectList.indexOf(hasData), 1);
          this.selectList.push(hasData);
          this.removeBoughtItem(hasData.id);
          this.addBoughtList(hasData);
          this.showListCount();
        }
      })

    }
    
    return this.selectList;
  }
  showListCount() {
    const countDiv = document.getElementById('productCounter');
    const totalPriceDiv = document.getElementById('totalPrice');
    const boughtProductsList = document.getElementById('ListTotal');
    const empytBag = document.getElementById('emptyBag');

    if (this.selectList.length === 0) {
      boughtProductsList.style.display = 'none';
      empytBag.style.display = 'block';
    } else {
      boughtProductsList.style.display = 'block';
      empytBag.style.display = 'none';
    }

    this.totalPrice = 0;
    let productCount = 0;
    this.selectList.map(item => {
      this.totalPrice += parseFloat(item.value);
      productCount += parseInt(item.count);
    });
    totalPriceDiv.innerText = `${this.totalPrice.toFixed(2)} TL`;
    countDiv.innerText = productCount;
  }
  async showResult(data) {
    let index = 0;
    if (this.searchActive) {
      for (let item of data) {
        this.createItemTitle(item.CategoryDisplayName);
        let indexY = 0;
        for (let pItem of item.Products) {
          this.createProducts(pItem, index, indexY);
          indexY++;
        }
        index++;
      }
      this.getSelectList();
    }
  }

  createItemTitle(info) {
    const resultList = document.getElementById('searchResult');
    const newTr = document.createElement("tr");
    const newTd = document.createElement("td");
    const newH2 = document.createElement("h2")
    const newContent = document.createTextNode(info);

    newTd.setAttribute('class', 'category');
    newH2.appendChild(newContent);
    newTd.insertBefore(newH2, null);
    newTr.insertBefore(newTd, null);
    resultList.insertBefore(newTr, null);
  }

  createProducts(info, index, indexY) {
    const categoryItem = document.getElementsByClassName('category')[index];
    const generalDiv = document.createElement('div');
    const newList = document.createElement('label');
    const newContent = document.createTextNode(`${info.DisplayName} / ${info.ListPrice} TL`);
    const newCheckInput = document.createElement('input');
    const newCountInput = document.createElement('input');

    newCheckInput.setAttribute('type', 'checkbox');
    newCheckInput.setAttribute('name', 'productList');
    newCheckInput.setAttribute('class', 'productCheckBox');
    newCheckInput.id = `productList${index}${indexY}`;
    newCheckInput.value = info.ListPrice;

    newCheckInput.setAttribute('dataname', info.DisplayName);
    newCheckInput.setAttribute('dataid', info.ProductId);
    newCheckInput.setAttribute('datacount', `productCount${index}${indexY}`);
    newList.setAttribute('for', `productList${index}${indexY}`);
    newList.setAttribute('class', 'productList');
    generalDiv.setAttribute('class', 'list-item');

    newCountInput.setAttribute('name', `productCount`);
    newCountInput.setAttribute('type', 'number');
    newCountInput.value = 0;
    newCountInput.id = `productCount${index}${indexY}`;

    newList.appendChild(newContent);

    generalDiv.insertBefore(newCheckInput, null);
    generalDiv.insertBefore(newCountInput, null);
    generalDiv.insertBefore(newList, null);
    categoryItem.insertBefore(generalDiv, null);
  }

  removeItem(item) {
    this.removeBoughtItem(item.id);
    const removeItem = this.selectList.find(s => s.id === item.id);
    const removeElement  = document.getElementById(item.elId);
    removeElement.checked = false;
    // console.log(removeElement);
    this.selectList.splice(this.selectList.indexOf(item.id), 1);
  }

  addBoughtList(item) {
    const boughtProductsDiv = document.getElementById('boughtProducts');
    const boughtItemDiv = document.createElement('div');
    const countEm = document.createElement('em');
    const deleteButton = document.createElement('button');

    const newContent = document.createTextNode(`${item.name} / ${item.value} TL `);
    const newCount = document.createTextNode(`(${item.count} Adet)`)
    const deleteContent = document.createTextNode('x');

    boughtItemDiv.setAttribute('class', 'list-item');

    boughtItemDiv.id = item.id;
    deleteButton.id = `${item.id}Button`;
    boughtItemDiv.appendChild(newContent);
    countEm.appendChild(newCount);
    deleteButton.onclick = () => {
      this.removeItem(item);
    }
    deleteButton.appendChild(deleteContent);


    boughtItemDiv.insertBefore(countEm, null);
    boughtItemDiv.insertBefore(deleteButton, null);
    boughtProductsDiv.insertBefore(boughtItemDiv, null);
  }

  removeBoughtItem(id) {
    const removeElement = document.getElementById(id);
    Element.prototype.remove = function () {
      this.parentElement.removeChild(this);
    }
    NodeList.prototype.remove = HTMLCollection.prototype.remove = function () {
      for (var i = this.length - 1; i >= 0; i--) {
        if (this[i] && this[i].parentElement) {
          this[i].parentElement.removeChild(this[i]);
        }
      }
    }

    removeElement.remove();
  }

}

const Foods = new Food();
Foods.getFoods();
function onSearch() {
  Foods.onSearch();
};